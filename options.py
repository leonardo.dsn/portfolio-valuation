'''
Leonardo de Sá Nicolazzi - Clube de Finanças

29/03/2020

Option valuation - Binomial Method
'''

import random as rd
import math
import pandas as pd
import statistics
import numpy as np

def histData(fileName):
    base = pd.read_csv(fileName, sep=',')
    base['variacao'] = (base['Price'] - base['Price'].shift(1))/base['Price']  # maior que 1 indica que subiu
    return base

def volatility(base):
    dev = []
    for i in range(1,len(base)):
        ap = base['variacao'][i]
        dev.append(ap)
    desv = statistics.stdev(dev)
    desvc = desv * ((252) ** (0.5))
    # print(desv)
    return desvc

def currentPrice(base, date):
    C = 0
    datex = str(date)
    for i in range(1, len(base)):
        base_data = str(base['Date'][i])
        if base_data == datex:
            C = base['Price'][i]
            continue
    return C

def optionValuation(S, C, t, r, type, asset, data):

    base = histData('./histData/'+ asset+'.csv')
    desv = volatility(base)

    C = currentPrice(base, data)

    u = math.e ** (((1+desv) ** ((t/252) ** 0.5))-1)
    d = 1/u
    p = (math.e**(r*(t/252))-d)/(u-d)#probabilidade de subir, prob de descer = 1-p

    optUp = C * u
    optDown = C * d
    x = (1 + r - d) / (u - d)

    if type == 'Call':
        callU = max(optUp-S, 0)
        callD = max(optDown-S, 0)
        value = (p*callU+((1-p)*callD))/((1+r)**(t/252))

    if type == 'Put':
        putU = max(S-optUp, 0)
        putD = max(S-optDown, 0)
        value = (p * putU + ((1 - p) * putD)) / ((1 + r)**(t/252))

    # print(value)

    return value
'base'
base = histData('./histData/BRFS3.csv')

'''factors'''
S = 30 #option strike price
C = 34 #current stock price
t = 33 #time until expiration in days
r = 0.03 #discount rate
type = 'Put'

desv = volatility(base)
desvc = desv*((252)**(0.5))

#optionValuation(S, C, desvc, t, r, type)