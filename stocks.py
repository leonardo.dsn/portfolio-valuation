'''
Leonardo de Sá Nicolazzi - Clube de Finanças

29/03/2020

Stock valuation - Binomial Method
'''

import random as rd
import math
import pandas as pd
import numpy as np

def histData(fileName):
    base = pd.read_csv(fileName, sep=',')
    base['variacao'] = (base['Price'] - base['Price'].shift(1))/base['Price']  # maior que 1 indica que subiu
    return base

def currentPrice(base, date):
    C = 0
    datex = str(date)
    for i in range(1, len(base)):
        base_data = str(base['Date'][i])
        if base_data == datex:
            C = base['Price'][i]
            continue
    return C

def stockValuation(asset, date, usd):
    file = './histData/'+ asset + '.csv'
    base = histData(file)

    C = currentPrice(base, date)

    if usd == True:
        base = histData('./histData/USD_BRL.csv')
        usdValue = currentPrice(base, date)
        C = C * usdValue
    return C


'''stock list'''

#stockList=