'''
Leonardo de Sá Nicolazzi - Clube de Finanças

29/03/2020

Bond Valuation - fixed coupon bonds
'''

import random as rd
import math
import pandas as pd
import numpy as np

'''Bond Info'''
F = 10000 # face value
ppy = 2 #payments per year
t = 10 #time to maturity in years
c = 0.05 # Coupon rate
r = 0.0375 # discount rate

def bondValue(F, ppy, t, c, r):
    C = c * F / ppy  # Periodic coupon payment
    n = ppy * t  # total payments

    value = (C*((1-(1+r/ppy)**(-n))/(r/ppy))+(F/(1+r/ppy)**n))
    return value

# print(bondValue(F,ppy,t,c,r))