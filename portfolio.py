'''
Leonardo de Sá Nicolazzi - Clube de Finanças

29/03/2020

Stock valuation - Binomial Method
'''

import random as rd
import math
import pandas as pd
import numpy as np
import bonds
import options
import stocks
#import matplotlib.a as plot

'''Parametros de Risco'''
r = 0.0375 #taxa livre de risco


# [F, ppy, t, c, r, percent]
def bondCreate(r):
    bondList = [[25000, 1, 6, 0.06, r, 0.000000005],
                # [150000, 4, 13, 0.055, r, 0.05],
                # [75000, 2, 3, 0.0775, r, 0.05],
                # [10000, 2, 10, 0.05, r, 0.05]
                ]
    return bondList

#S, C(n importa), t, r, type, asset, percent
def optionCreate(r):
    optionList = [[35, 97.4, 252, r, 'Put', 'BBAS3',0.00000002],
                  [25, 33, 599, r, 'Put', 'LAME4',0.000000002],
                  # [23, 15, 290, r, 'Put', 'ABEV3',0.02],
                  # [47, 55, 555, r, 'Put', 'VALE3',0.02]
                  ]
    return optionList

#asset, percent, usd = false
def stockCreate(r):
    stockList = [['ABEV3', False, 0.35/13],
                 ['BBAS3', False, 0.35/13],
                 ['BRFS3', False, 0.35/13],
                 ['CPLE6', False, 0.35/13],
                 ['EGIE3', False, 0.35/13],
                 ['JBSS3', False, 0.35/13],
                 ['LAME4', False, 0.35/13],
                 ['MRFG3', False, 0.35/13],
                 ['RENT3', False, 0.35/13],
                 ['VALE3', False, 0.35/13],
                 ['WEGE3', False, 0.35/13],
                 ['SAPR4', False, 0.35/13],
                 ['SUZB3', False, 0.35/13],
                 ['LTN010123', False, 0.25],
                 ['NTF010127', False, 0.25],
                 ['USD_BRL', False, 0.0375],
                 # ['JPY_BRL', False, 0.0375],
                 ['Gold', True, 0.0375],
                 ['SPX', True, 0.075]
                ] # 13 acoes, usd, spx, iene

                    #LTN = PREFIXADA, NTN-B = IPCA, LFT = SELIC
    return stockList

'''Bond Valuation'''
def bondsValuation(bondList, data):
    bondValue = []
    bondn = len(bondList)
    for i in range(0, bondn):
        v = bonds.bondValue(bondList[i][0],bondList[i][1],bondList[i][2],bondList[i][3], bondList[i][4])
        bondValue.append(v)
    # print(bondValue)
    return bondValue

'''Option Valuation'''

def optionsValuation(optionList, data):
    optionValue = []
    optionn = len(optionList)
    for i in range(0,optionn):
        v = options.optionValuation(optionList[i][0],optionList[i][1],optionList[i][2],optionList[i][3],optionList[i][4],optionList[i][5], data)
        optionValue.append(v)
    # print(optionValue)
    return optionValue

'''Stock Valuation'''

def stocksValuation(stockList,data):
    stockValue = []
    stockn = len(stockList)
    for i in range(0,stockn):
        v = stocks.stockValuation(stockList[i][0],data,stockList[i][1])
        stockValue.append(v)
    # print(stockValue)
    return stockValue

def stressTest(initDate, finalDate):
    '''Portfolio Value Calcs'''
    # dates = 9/03, 12/03, 16/03

    r=0.05
    bondList = bondCreate(r)
    stockList = stockCreate(r)
    optionList = optionCreate(r)
    initOption = optionsValuation(optionList, initDate)
    initStock = stocksValuation(stockList, initDate)
    initBond = bondsValuation(bondList, initDate)


    r=0.084
    bondList = bondCreate(r)
    stockList = stockCreate(r)
    optionList = optionCreate(r)
    endOption = optionsValuation(optionList, finalDate)
    endStock = stocksValuation(stockList, finalDate)
    endBond = bondsValuation(bondList, finalDate)

    '''Delta Portfolio'''

    initialPortfolio = 0
    pos = 0
    for i in range(0,len(initBond)):
        initialPortfolio += endBond[i]/initBond[i]*bondList[i][5]
        pos += bondList[i][5]

    for i in range(0,len(initOption)):
        initialPortfolio += endOption[i]/initOption[i]*optionList[i][6]
        pos += optionList[i][6]

    for i in range(0,len(initStock)):
        initialPortfolio += endStock[i]/initStock[i]*stockList[i][2]
        pos += stockList[i][2]
        test = endStock[i]/initStock[i]
        num = int((test - 1) * 100 * 10000) / 10000
        print(str(num) + '% on ' +stockList[i][0] )


    # print(initialPortfolio)
    # print(pos)
    return initialPortfolio/pos

def printRes(data1, data2):
    test = stressTest(data1, data2)
    num = int((test-1)*100*10000)/10000
    print(str(num) + '% on ' + data2)

# dates = 9/03, 12/03, 16/03

data1 = 'Mar 06, 2020'
data2 = 'Mar 09, 2020'
printRes(data1, data2)

data1 = 'Mar 11, 2020'
data2 = 'Mar 12, 2020'
printRes(data1, data2)

# data1 = 'Mar 12, 2020'
# data2 = 'Mar 13, 2020'
# printRes(data1, data2)

data1 = 'Mar 13, 2020'
data2 = 'Mar 16, 2020'
printRes(data1, data2)

data1 = 'Mar 17, 2020'
data2 = 'Mar 18, 2020'
printRes(data1, data2)

# data1 = 'Mar 23, 2020'
# data2 = 'Mar 24, 2020'
# printRes(data1, data2)



#DONE = PRECO de strike como C da option
#DONE = Stock valuation, init date and end date
#DONE = Situacao final e situacao inicial valuation


#TODO = Variacao do t na option
#TODO = Corrigir option valuation

