'''
Leonardo de Sá Nicolazzi - Clube de Finanças

29/03/2020

Stock valuation - Binomial Method
'''

import pandas as pd
from pandas_datareader import data as pdr
# import fix_yahoo_finance as yf
import numpy as np
import datetime as dt
from scipy.stats import norm
import yfinance as yf

# Create our portfolio of equities

tickers = ['ABEV3.SA',
           'BBAS3.SA',
           'BRFS3.SA',
           'CPLE6.SA',
           'EGIE3.SA',
           'JBSS3.SA',
           'LAME4.SA',
           'MRFG3.SA',
           'RENT3.SA',
           'SAPR4.SA',
           'SUZB3.SA',
           'VALE3.SA',
           'WEGE3.SA',
           'BRL=X',
           'JPYBRL=X',
           '^GSPC']

# Set the investment weights (I arbitrarily picked for example)
weights = np.array([0.35/13, 0.35/13, 0.35/13, 0.35/13,0.35/13,0.35/13,0.35/13,0.35/13,0.35/13,0.35/13,0.35/13,0.35/13,0.35/13,
                    0.075, 0, 0.075])

# Set an initial investment level
initial_investment = 1000000

# Download closing prices
data = pdr.get_data_yahoo(tickers, start="2015-01-01", end=dt.date.today())['Close']

# From the closing prices, calculate periodic returns
returns = data.pct_change()
returns.tail()

# Generate Var-Cov matrix
cov_matrix = returns.cov()
print(cov_matrix)

# Calculate mean returns for each stock
avg_rets = returns.mean()

# Calculate mean returns for portfolio overall,
# using dot product to
# normalize individual means against investment weights
# https://en.wikipedia.org/wiki/Dot_product#:~:targetText=In%20mathematics%2C%20the%20dot%20product,and%20returns%20a%20single%20number.
port_mean = avg_rets.dot(weights)

# Calculate portfolio standard deviation
port_stdev = np.sqrt(weights.T.dot(cov_matrix).dot(weights))

# Calculate mean of investment
mean_investment = (1 + port_mean) * initial_investment

# Calculate standard deviation of investmnet
stdev_investment = initial_investment * port_stdev

# Select our confidence interval (I'll choose 95% here)
conf_level1 = 0.1586

# Using SciPy ppf method to generate values for the
# inverse cumulative distribution function to a normal distribution
# Plugging in the mean, standard deviation of our portfolio
# as calculated above
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.norm.html
cutoff1 = norm.ppf(conf_level1, mean_investment, stdev_investment)

#Finally, we can calculate the VaR at our confidence interval
var_1d1 = initial_investment - cutoff1
print(var_1d1)

#output
#22347.7792230231